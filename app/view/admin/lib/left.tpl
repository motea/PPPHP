    <div class="pure-u">
        <div class="side">
            <div class="pure-menu pure-menu-open side-menu">
                <ul>
                    <li class="pure-menu-heading side-menu-head"><i class="icon-tags"></i> 核心组件</li>
                    <li class="pure-menu-selected"><a href="/admin/index">基本效果</a></li>
                    <li><a href="/admin/form">表单功能</a></li>
                    <li><a href="/admin/lists">表格功能</a></li>
                    <li><a href="/admin/js">动态效果</a></li>
                    <li class="pure-menu-heading side-menu-head"><i class="icon-tags"></i> 独立页面</li>
                    <li><a href="/admin/error" target="_blank">错误页</a></li>
                    <li><a href="/admin/login" target="_blank">登录页</a></li>
                </ul>
            </div>
        </div>
    </div>